gnupginterface (0.3.2-10) UNRELEASED; urgency=low

  [ Piotr Ożarowski ]
  * Added Vcs-Svn and Vcs-Browser fields

  [ Sandro Tosi ]
  * fixed Vcs-Browser field
  * debian/control
    - switch Vcs-Browser field to viewsvn
  * Use the new Debian Python Team contact name and address

  [ Carlos Galisteo ]
  * debian/control
    - Added Homepage field.

  [ Jakub Wilk ]
  * Use canonical URIs for Vcs-* fields.

  [ Ondřej Nový ]
  * Fixed VCS URL (https)
  * d/control: Set Vcs-* to salsa.debian.org
  * d/changelog: Remove trailing whitespaces
  * d/control: Remove trailing whitespaces
  * Remove debian/pycompat, it's not used by any modern Python helper
  * Convert git repository from git-dpm to gbp layout
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

 -- Sandro Tosi <morph@debian.org>  Mon, 04 Jan 2021 16:59:41 -0500

gnupginterface (0.3.2-9.1) unstable; urgency=low

  * Non-maintainer upload.
  * Merge in Ubuntu patches to address exit status bug, thanks!
  * Move setup.py patch from diff.gz to patch system.

 -- Thijs Kinkhorst <thijs@debian.org>  Fri, 18 Dec 2009 13:34:48 +0100

gnupginterface (0.3.2-9ubuntu2) jaunty; urgency=low

  * Add 01__print_exit_status_correctly.patch (Closes: #509415, LP: #333057)
    Thanks to Kenneth Loafman for the initial patch.

 -- Brian Murray <brian@ubuntu.com>  Tue, 31 Mar 2009 14:48:28 -0700

gnupginterface (0.3.2-9ubuntu1) gutsy; urgency=low

  * Rebuild to remove the .pyc file (LP: #36733)
  * debian/control:
    + Update to current python-support usage.
    + Modify Maintainer value to match DebianMaintainerField spec.
  * setup.py: Rename the licence field to license as suggested by the warning.

 -- Michael Bienia <geser@ubuntu.com>  Fri, 24 Aug 2007 01:47:09 +0200

gnupginterface (0.3.2-9) unstable; urgency=low

  [ Piotr Ozarowski ]
  * Added watch file.

  [ Gustavo Franco ]
  * debian/rules: Add simple-patchsys support without patches.

  * New Python Policy Changes: (Closes: #373438)
    + debian/control:
      - Bump up cdbs dependency to >= 0.4.41.
      - Add python-support as Build-Depends-Indep.
      - Add XS-Python-Version field.
      - Depends on ${python:Depends} and not on python2.3.
      - Add XB-Python-Version field.
    + debian/rules:
      - Add DEB_PYTHON_SYSTEM=pysupport.
      - Remove python-support related entries since cdbs take care
        of this now.
    + debian/postinst && debian/prerm:
      - Remove "hand made" python-support stuff, cdbs does this.

 -- Gustavo Franco <stratus@debian.org>  Thu, 15 Jun 2006 17:05:55 -0300

gnupginterface (0.3.2-8) unstable; urgency=low

  * python-support added to provide this module for multiple
    python versions
  * debian/control:
    - Add Gustavo Franco to Uploaders
    - Update Standards-Version to 3.7.2 with no changes
    - Update debhelper depends to 5
    - Move cdbs and debhelper to Build-Depends field
    - Relax python dependencies due to multiple versions support

 -- Gustavo Franco <stratus@debian.org>  Fri, 26 May 2006 18:59:07 -0300

gnupginterface (0.3.2-7) unstable; urgency=low

  * New maintainer (Closes: #305461)
  * Create a doc-base entry for the package
  * Fix setup.py to use 'license' instead of 'licence'
  * debian/control:
    - Changed section to 'python'
    - Bumped Standards-Version to 3.6.1.1
    - Build dependencies
      + Add build-dependency on cdbs
      + Reduce versioned dependency on debhelper to 4.1.0
      + Remove python and python2.3-dev: only python-dev should suffice
  * debian/rules:
    - Fully converted to CDBS
    - No longer exports DH_COMPAT

 -- Guilherme de S. Pastore <gpastore@colband.com.br>  Fri, 22 Apr 2005 09:10:13 -0300

gnupginterface (0.3.2-6) unstable; urgency=low

  * Orphaning this package.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 19 Apr 2005 23:19:22 -0500

gnupginterface (0.3.2-5) unstable; urgency=low

  * Apply patch from Matthias Ulrichs <smurf@debian.org> to
    fix error reporting if/when gnupg dies. (Closes: #226462)

 -- John Goerzen <jgoerzen@complete.org>  Mon, 28 Mar 2005 06:44:13 -0600

gnupginterface (0.3.2-4) unstable; urgency=low

  * Added build-dep on python.  Closes: #259115.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 13 Jul 2004 10:52:00 -0500

gnupginterface (0.3.2-3) unstable; urgency=low

  * ACK NMU.  Closes: #213109, #205394, #180495.
  * Call setup.py clean in debian/rules clean target and rm -rf build.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 21 Oct 2003 22:23:50 -0500

gnupginterface (0.3.2-2.1) unstable; urgency=low

  * NMU.
  * Build package in the binary-indep target.
  * Removed the example scripts in the debian directory.
  * Use postinst/prerm to handle byte compiled files (closes: #205394).
  * Remove empty directories from binary package (closes: #180495).

 -- Matthias Klose <doko@debian.org>  Sun, 28 Sep 2003 14:30:30 +0200

gnupginterface (0.3.2-2) unstable; urgency=low

  * Updated to use Python 2.3.

 -- John Goerzen <jgoerzen@complete.org>  Tue, 12 Aug 2003 13:15:50 -0500

gnupginterface (0.3.2-1) unstable; urgency=low

  * Initial Release.  Closes: #168995.

 -- John Goerzen <jgoerzen@complete.org>  Thu, 14 Nov 2002 11:22:58 -0600
